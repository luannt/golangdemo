module golangdemo

go 1.15

require (
	github.com/boumenot/gocover-cobertura v1.2.0 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.1
	go.uber.org/fx v1.13.1
)
