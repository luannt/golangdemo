package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"
	"golangdemo/internal/configfx"
	"golangdemo/internal/ginfx"
	"net/http"
	"os"
)

func main() {
	app := fx.New(
		configfx.Initialize("demoservice", "service", "configs"),
		ginfx.Module,
		fx.Invoke(startService),
		fx.Invoke(register))
	app.Run()
}

func register(engine *gin.Engine) {
	g := engine.Group("")
	{
		g.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"data": "hello world"})
		})
	}

}
func startService(engine *gin.Engine, lifecycle fx.Lifecycle) {
	lifecycle.Append(fx.Hook{
		OnStart: func(c context.Context) error {
			port := viper.GetString("port")
			logrus.Info("run with port from config file: ", port)
			port = os.Getenv("PORT")
			logrus.Info("run with port from OS: ", port)
			logrus.Infof("[SERVER STARTING] starting service, run on port: %s", port)
			go startServer(engine, port)
			return nil
		},
		OnStop: func(c context.Context) error {
			logrus.Info("[SERVER STOPPING] stopping service")
			return nil
		},
	})
}

func startServer(handler http.Handler, port string) {
	server := http.Server{
		Handler: handler,
		Addr:    ":" + port,
	}
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logrus.WithField("error", err).Error("failed to listen and serve server")
	}
}
