package ginfx

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
)

var Module = fx.Provide(provideGinEngine)

func provideGinEngine() *gin.Engine {
	return gin.Default()
}