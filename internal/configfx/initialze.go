package configfx

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"
	"strings"
)

func Initialize(svcName, cfgFile, cfgPath string) fx.Option {
	return fx.Invoke(func() {
		logrus.Infof("Starting %s\n", viper.GetString("title"))
		loadConfiguration(svcName, cfgFile, cfgPath)
	})
}

func loadConfiguration(serviceName, configFile, configPath string) {
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()

	logrus.Info("Loading configuration from local (file system or os env)")
	logrus.Infof("configFile [%s] configPath [%s]", configFile, configPath)

	viper.SetConfigName(configFile)
	viper.AddConfigPath(configPath)
	viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", serviceName))
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		logrus.Errorf("Can't load configuration from local file system, error: %s", err)
		return
	}
	logrus.Infof("Using config file: %s", viper.ConfigFileUsed())
	logrus.Info("Loaded configuration from local file system successfully")
}
