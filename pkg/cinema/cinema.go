package cinema

import "fmt"

type Manager interface {
}
type Cinema struct {
	RowNum    int
	ColumnNum int
	Matrix    [][]Seat
	Distance  int
}

type Seat struct {
	Row      int
	Column   int
	IsPicked bool
}

func (s *Seat) name() string {
	return fmt.Sprintf("%d-%d", s.Row, s.Column)
}

func NewCinema(rows, columns int, matrix [][]Seat, distance int) Manager {
	return &Cinema{
		RowNum:    rows,
		ColumnNum: columns,
		Matrix:    matrix,
		Distance:  distance,
	}
}

var (
	directions = []struct {
		Row, Column int
	}{
		{0, -1}, {0, 1},
		{1, 0}, {-1, 0},
	}
)

func (c *Cinema) AdjacentSeat(seat Seat) []Seat {
	adjacentSeats := make([]Seat, 0, len(directions))
	for _, d := range directions {
		row, column := seat.Row+d.Row, seat.Column+d.Column
		if c.InvalidSeat(row, column) {
			continue
		}
		adjSeat := c.Matrix[row][column]
		adjacentSeats = append(adjacentSeats, adjSeat)
	}
	return adjacentSeats
}

func (c *Cinema) InvalidSeat(row, column int) bool {
	return row < 0 || row >= c.RowNum || column < 0 || column >= c.ColumnNum
}

func (c *Cinema) GetAvailableSeats(seatSet int) [][]Seat {
	availableSeats := make([]Seat, 0, c.RowNum*c.ColumnNum)
	for _, seats := range c.Matrix {
		for _, seat := range seats {
			fmt.Println("-------------------------------------")
			fmt.Println("check seat: ", seat)
			if seat.IsPicked {
				fmt.Println("seat is picked, continue ", seat)
				continue
			}
			fmt.Println("start traversal seat: ", seat)
			if c.Traversal(seat) {
				fmt.Println("seat available, append it ", seat)
				availableSeats = append(availableSeats, seat)
			}
		}
	}
	fmt.Println("available seats:", availableSeats)
	return c.GetSeatSetAvailable(availableSeats, seatSet)
}

func (c *Cinema) GetSeatSetAvailable(seats []Seat, set int) (seatSet [][]Seat) {
	if len(seats) < set {
		fmt.Println("return nil by len seats < set", len(seats), set)
		return nil
	}

	fmt.Println("------------------- start")
	current := make([]Seat, 0, set)
	c.SeatDFS(seats, &seatSet, &current, 0, set)
	fmt.Println("------------------- stop")

	fmt.Println("seat set after traversal", seatSet)
	return
}

func (c *Cinema) SeatDFS(seats []Seat, seatSet *[][]Seat, current *[]Seat, index int, set int) {
	fmt.Println("start DFS", "current: ", current, "index:", index, "set:", set)
	if set == 0 {
		combination := make([]Seat, len(*current))
		copy(combination, *current)
		if c.isAdjacentSeats(combination) {
			*seatSet = append(*seatSet, combination)
		}
		//*seatSet = append(*seatSet, combination)
		fmt.Println("seat set:", seatSet)
		return
	}
	fmt.Println("start loop ---- ")
	for i := index; i < len(seats); i++ {
		fmt.Println("i: ", i)
		*current = append(*current, seats[i])
		c.SeatDFS(seats, seatSet, current, i+1, set-1)
		fmt.Println("current before popping:", current, "index:", index, "i:", i)
		l := len(*current)
		arr := *current
		a := arr[:l-1]
		*current = a
		fmt.Println("current: ", current, "i:", i)
	}
	fmt.Println("----stop DFS")
}

func (c *Cinema) isAdjacentSeats(seats []Seat) bool {
	if len(seats) == 1{
		return true
	}
	for _, s := range seats {
		adjacentSeats := c.AdjacentSeat(s)
		count := 0
		for _, seat := range seats {
			if s.name() == seat.name() {
				continue
			}
			for _, adjacentSeat := range adjacentSeats {
				if seat.name() == adjacentSeat.name() {
					count++
				}
			}
		}
		if count == 0 {
			fmt.Println("return false by count == 0")
			return false
		}
	}
	return true
}

func (c *Cinema) Traversal(seat Seat) bool {
	visitedSet := map[string]bool{}
	distance := 0
	return c.traversal(visitedSet, seat, distance)
}

func (c *Cinema) traversal(visitedSet map[string]bool, seat Seat, distance int) bool {
	fmt.Println("----------")
	fmt.Println("traversal:", seat, "visited set:", visitedSet, "distance:", distance)
	if distance > c.Distance {
		fmt.Println("return true by distance statistic: ", seat)
		return true
	}
	if seat.IsPicked {
		statistic := distance > c.Distance
		fmt.Printf("return %v by seat is picked but distance %d: \n", statistic, distance)
		return statistic
	}
	visitedSet[seat.name()] = true
	adjacentSeats := c.AdjacentSeat(seat)
	fmt.Println("list adjacent seats: ", adjacentSeats)
	for _, adjacentSeat := range adjacentSeats {
		if visited := visitedSet[adjacentSeat.name()]; visited {
			fmt.Println("continue by seat is visited: ", seat)
			continue
		}
		if c.traversal(visitedSet, adjacentSeat, distance+1) {
			fmt.Println("continue by seat is met condition: ", seat)
			continue
		}
		fmt.Println("return false traversal adjacent seat unavailable", seat)
		return false
	}
	return true
}
