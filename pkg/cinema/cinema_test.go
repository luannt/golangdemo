package cinema

import (
	"fmt"
	"testing"
)

func TestCinema(t *testing.T) {
	matrix := [][]Seat{
		{Seat{0, 0, false}, Seat{0, 1, false}, Seat{0, 2, true}},
		{Seat{1, 0, false}, Seat{1, 1, false}, Seat{1, 2, false}},
		{Seat{2, 0, false}, Seat{2, 1, false}, Seat{2, 2, false}},
	}
	cinema := &Cinema{3, 3, matrix, 3}
	result := cinema.GetAvailableSeats(1)
	fmt.Println("list available: ", result)
}
