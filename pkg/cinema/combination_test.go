package cinema

import (
	"fmt"
	"testing"
)

type CombinationIterator struct {
	combinations []string
}

func Constructor(characters string, combinationLength int) CombinationIterator {
	var res []string
	dfs([]byte(characters), &res, combinationLength, []byte{}, 0)
	return CombinationIterator{res}
}

func dfs(chars []byte, res *[]string, n int, cur []byte, pos int) {
	fmt.Println("chars:", string(chars), "res:", res, "n:", n, "cur:", string(cur), "pos:", pos)
	if n == 0 {
		*res = append(*res, string(cur))
		fmt.Println("response:", res)
		return
	}
	for i := pos; i < len(chars); i++ {
		cur = append(cur, chars[i])
		fmt.Println("call dfs","chars: ",string(chars),"res:",res,"n:",n-1,"cur",string(cur),"pos:", i+1)
		dfs(chars, res, n-1, cur, i+1)
		cur = cur[:len(cur)-1]
		fmt.Println("cur: ", string(cur) )
	}
}

func TestCombination(t *testing.T) {
	a := Constructor("ABCD", 3)
	fmt.Println(a)
}
